This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

## npx create-next-app name_app

## VERCEL DEPLOYMENT
## everything goes to Bitbucket
## git init
## git add .
## git commit -m 'First commit'
## Create a new repository on BitBucket
## Copy the clone URL
## git clone --mirror https://alonsoramirez2021@bitbucket.org/alonsoramirez2021/nextjs_djevents_frontend.git
## git remode add bitbucket https://alonsoramirez2021@bitbucket.org/alonsoramirez2021/nextjs_djevents_frontend.git
## git push --all bitbucket

## git fetch --prune 
## check the main branch
## go to Vercel, log in with BitBucket account and make sure the project contains the code
## select in BitBucket the main branch as the root branch
## in vercel, choose personal account and when it comes to environment variables
## NEXT_PUBLIC_API_URL = Strapi backend production url   e.g. https://djeventsbackend2021.herokuapp.com/
## NEXT_PUBLIC_FRONTEND_URL = Vercel frontend production url e.g. https://djeventsfrontend2021.vercel.app
## copy the .env local info into your env variables
## hit deploy button