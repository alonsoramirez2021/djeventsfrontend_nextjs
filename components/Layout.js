import Head from 'next/head';
import {useRouter} from 'next/router';
import Header from './Header';
import Footer from './Footer';
import Showcase from './Showcase';
import styles from '@/styles/Layout.module.css';

//The general template for the whole project
export default function Layout({title, keywords, description, children}) {
    const router = useRouter();
    return (
        <div>
           <Head>
                <title>{title}</title>
                <meta name = 'keywords' content={keywords}/>
                <meta name = 'description' content={description}/>
            </Head> 

            <Header/>
            {/* This help us to show the image only on the home page and not on the other. This is a shorthand if else */}
            {router.pathname === '/' && <Showcase/>}
            <div className={styles.container}>
            {children}
            </div>
            <Footer/>
        </div>
    )
}


Layout.defaultProps = {
    title: 'DJ Events | My first Next.js app',
    description: 'Find the latest DJ and other musical events',
    keywords: 'music,dj,edm,events'
}