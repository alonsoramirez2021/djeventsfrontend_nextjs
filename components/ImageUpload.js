import { useState } from 'react'
import { API_URL } from '@/config/index'
import styles from '@/styles/Form.module.css'

export default function ImageUpload({ evtId, imageUploaded,token }) {
    //we assume no photo has been uploaded
  const [image, setImage] = useState(null)

  const handleSubmit = async (e) => {
      //preventing from submitting tue form
    e.preventDefault()
    const formData = new FormData()
    formData.append('files', image)
    formData.append('ref', 'events')
    formData.append('refId', evtId)
    formData.append('field', 'image')
        //the new url to upload our photo
    const res = await fetch(`${API_URL}/upload`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`
      },
      body: formData,
    })

    if (res.ok) {
      imageUploaded()
    }
  }
//handling the change of image
  const handleFileChange = (e) => {
    setImage(e.target.files[0])
  }
//form for uploading the photo
  return (
    <div className={styles.form}>
      <h1>Upload Event Image</h1>
      <form onSubmit={handleSubmit}>
        <div className={styles.file}>
          <input type='file' onChange={handleFileChange} />
        </div>
        <input type='submit' value='Upload' className='btn' />
      </form>
    </div>
  )
}