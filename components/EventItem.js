import Link from 'next/link';
import Image from 'next/image';
import styles from '@/styles/EventItem.module.css';

export default function EventItem({evt}) {
   return (
        //This is how every item will be shown
        <div className={styles.event}>
            <div className={styles.img}>
                {/*If there is not an image, sets the default image for the event*/}
                <Image
                src ={evt.image 
                    ? evt.image.formats.thumbnail.url 
                    : '/images/event-default.png'}
                width={170}
                height={100}
                />
            </div>
            {/*Display the details of the event*/}
            <div className={styles.info}>
                <span>
                    {new Date(evt.date).toLocaleDateString('en')} at {evt.time}
                </span>
                <h3>{evt.name}</h3>
            </div>
            {/*Display the button with the slug*/}
            <div className={styles.link}>
                <Link href={`/events/${evt.slug}`}>
                    <a className='btn'>Details</a>
                </Link>
            </div>
        </div>
    )
}
