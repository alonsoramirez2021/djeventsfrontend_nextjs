import {useState} from 'react'
import {useRouter} from 'next/router'
import styles from '@/styles/Search.module.css'

export default function Search() {
    //const [state, setstate] = useState(initialState)
    const [term, setTerm] = useState('')
    //Get the router object
    const router = useRouter()

    //Navigate with router.push() and handling the submit based on the event
    const handleSubmit = (e) => {
        e.preventDefault()
        router.push(`/events/search?term=${term}`)
        setTerm('')
    }
    return (
        <div className={styles.search}>
            {/*The term changes according to the text*/}
            <form onSubmit={handleSubmit}>
                <input
                type='text'
                value={term}
                onChange={(e)=>setTerm(e.target.value)}
                placeholder='Search Events'
                />
            </form>
        </div>
    )
}
