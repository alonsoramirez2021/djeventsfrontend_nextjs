import Image from 'next/image'
import { useState, useEffect } from 'react'
import ReactMapGl, { Marker } from 'react-map-gl'
import 'mapbox-gl/dist/mapbox-gl.css'
import Geocode from 'react-geocode'

export default function EventMap({evt}) {
    //setting latitude and longitude
    const [lat, setLat] = useState(null)
    const [lng, setLng] = useState(null)
    //loading map
    const [loading, setLoading] = useState(true)
    //setting the viewport for the map
    const [viewport, setViewport] = useState({
        //this initial latitude and longitude values will be changed by the real values
        latitude: 40.712772,
        longitude: -73.935242,
        width: '100%',
        height: '500px',
        zoom: 12,
      })

      //after render
      useEffect(() => {
        // Get latitude & longitude from address.
        Geocode.fromAddress(evt.address).then(
          (res) => {
              //get real latitud and longitude values
            const { lat, lng } = res.results[0].geometry.location
            //setting the values
            setLat(lat)
            setLng(lng)
            //setting the new viewport
            setViewport({ ...viewport, latitude: lat, longitude: lng })
            setLoading(false)
          },
          (error) => {
            console.error(error)
          }
        )
      }, [])

      //SETTING THE KEY FOR GOOGLE MAPS
      Geocode.setApiKey(process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY)
      //this is for avoiding anything before the map is loaded
      if (loading) return false

      //console.log(lat,lng)
    return (
        <div>
            <h1>Maps</h1>
            {/*Setting for Mapbox display through its API access token */}
            <ReactMapGl
            {...viewport}
            mapboxApiAccessToken={process.env.NEXT_PUBLIC_MAPBOX_API_TOKEN}
            onViewportChange={(vp) => setViewport(vp)}
            >
            {/*Setting a marker for the current address with the image given on the folder*/}
            <Marker key={evt.id} latitude={lat} longitude={lng}>
                <Image src='/images/pin.svg' width={30} height={30} />
            </Marker>
            </ReactMapGl>
        </div>
    )
}

//npm i react-geocode@latest react-map-gl@latest

//Don't forget to enable Directions API,Geocoding API,Maps Javascript API 
//https://console.cloud.google.com/apis/library?project=project-id

//Don't forget to link a billing account