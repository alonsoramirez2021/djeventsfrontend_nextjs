import { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import { FaTimes } from 'react-icons/fa'
import styles from '@/styles/Modal.module.css'

export default function Modal({ show, onClose, children, title }) {
  
    /*On the initial load of the page, the rendering happens on the server, 
    where the window.document object is not available, so we will get an error that it is undefined. 
    So we need to make sure that, we use the document object safely only in the browser environment. 
    The easiest way to achieve this is by adding isBrowser boolean and set it to true in the initial render of the component.
    This can be done using the useEffect hook.*/
  const [isBrowser, setIsBrowser] = useState(false)

  useEffect(() => setIsBrowser(true))

  const handleClose = (e) => {
     //this prevents from closing the modal
    e.preventDefault()
    //onClose js method
    onClose()
  }


//decides to be shown if show is true, otherwise it will be set to null
  const modalContent = show ? (
    <div className={styles.overlay}>
      <div className={styles.modal}>
        <div className={styles.header}>
          <a href='' onClick={handleClose}>
            <FaTimes />
          </a>
        </div>
        {title && <div>{title}</div>}
        <div className={styles.body}>{children}</div>
      </div>
    </div>
  ) : null

  //save the reference to our modal-root DOM object
  //The first argument of the createPortal function is the root component and 
  //the second argument is the reference to the DOM object where it will be rendered.
  if (isBrowser) {
    return ReactDOM.createPortal(
        //shows the modal content
      modalContent,
      document.getElementById('modal-root')
    )
  } else {
    return null
  }
}
