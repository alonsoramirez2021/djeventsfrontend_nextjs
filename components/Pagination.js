import Link from 'next/link'
import { PER_PAGE } from '@/config/index'

//pagination
export default function Pagination({ page, total }) {
    //we set the total of results per page
  const lastPage = Math.ceil(total / PER_PAGE)
  //we consider thev two cases with previous or next link
  return (
    <>
      {page > 1 && (
        <Link href={`/events?page=${page - 1}`}>
          <a className='btn-secondary'>Prev</a>
        </Link>
      )}

      {page < lastPage && (
        <Link href={`/events?page=${page + 1}`}>
          <a className='btn-secondary'>Next</a>
        </Link>
      )}
    </>
  )
}