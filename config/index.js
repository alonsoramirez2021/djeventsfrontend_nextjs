//export const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:3000'
export const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337'
//TOTAL OF PAGES TO BE DISPLAYED IN THE PAGINATION
export const PER_PAGE = 2
//for auth context
export const NEXT_URL = process.env.NEXT_PUBLIC_FRONTEND_URL || 'http://localhost:3000'