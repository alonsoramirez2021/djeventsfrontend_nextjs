import Link from 'next/link';
import Layout from '@/components/Layout';
import EventItem from '@/components/EventItem'
import { API_URL } from '@/config/index';

export default function HomePage({events}) {
  //as a static property, {events} can be used at any moment on the default function
  return (
    <Layout>
      <h1>Upcoming Events</h1>
      {/*if there are no events, shows a message*/}
      {events.length===0 && <h3>No events to show</h3>}

      {/*Map the JSON data and display every property that belongs to events */}
      {events.map((evt) => (
        <EventItem key={evt.id} evt={evt} />
      ))}

      {/*If there is at least one event, we get a link to view all events*/}
      {events.length > 0 && (
        <Link href='/events'>
          <a className='btn-secondary'>View All Events</a>
        </Link>
      )}
    </Layout>
  )
}

export async function getStaticProps(){
  //fetching data through the api route with its url and sorts by ascending date while shows 3 events
  //const res = await fetch(`${API_URL}/api/events`)
  const res = await fetch(`${API_URL}/events?_sort=date:ASC&_limit=3`)
  //contains all the json data
  const events = await res.json()

  return{
    //object with the props that will be received by the page component
    //shows only 3 events
    //props: {events},
    props: {events},
    //optional amount in seconds after which a page re-generation can occur
    revalidate: 1,
  }
}