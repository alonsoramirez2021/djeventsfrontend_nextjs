import cookie from 'cookie'
import { API_URL } from '@/config/index'

//persisting the user
export default async (req, res) => {
    //we check to get a get method
  if (req.method === 'GET') {
      //we check if there is a cookie or not
      //appears if someone public enters the page
    if (!req.headers.cookie) {
      res.status(403).json({ message: 'Not Authorized' })
      return
    }
    //we check the cookie so we can parse it and send to strapi
    const { token } = cookie.parse(req.headers.cookie)
    //we fetch the url
    const strapiRes = await fetch(`${API_URL}/users/me`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    //we get the strapi data
    const user = await strapiRes.json()

    if (strapiRes.ok) {
      res.status(200).json({ user })
    } else {
      res.status(403).json({ message: 'User forbidden' })
    }
  } else {
      //this is the only action that needs a get method
    res.setHeader('Allow', ['GET'])
    res.status(405).json({ message: `Method ${req.method} not allowed` })
  }
}