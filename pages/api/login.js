import cookie from 'cookie'
import { API_URL } from '@/config/index'
//this is for login
export default async (req, res) => {
    //checks if there is a POST method
  if (req.method === 'POST') {
      //checks the email and the password
    const { identifier, password } = req.body
    //connects with strapi
    const strapiRes = await fetch(`${API_URL}/auth/local`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        identifier,
        password,
      }),
    })

    const data = await strapiRes.json()
    

    if (strapiRes.ok) {
      // Set Cookie
      //Store JWT in Server HttpOnly Cookie
      res.setHeader(
        'Set-Cookie',
        cookie.serialize('token', data.jwt, {
          httpOnly: true,
          secure: process.env.NODE_ENV !== 'development',
          maxAge: 60 * 60 * 24 * 7, // 1 week
          sameSite: 'strict',
          path: '/',
        })
      )
        //setting the user
      res.status(200).json({ user: data.user })
      
    } else {
      res
        .status(data.statusCode)
        .json({ message: data.message[0].messages[0].message })
    }
  } else {
    res.setHeader('Allow', ['POST'])
    res.status(405).json({ message: `Method ${req.method} not allowed` })
  }
}