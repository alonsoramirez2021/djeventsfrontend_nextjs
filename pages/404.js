//main template and @ as our alias to substitute all the dots
import Layout from '@/components/Layout';
import Link from 'next/link';
//css
import styles from '@/styles/404.module.css';
//icons
import {FaExclamationTriangle} from 'react-icons/fa';

//overrides the current 404 page
export default function NotFoundPage() {
    return (
        <Layout title='Page Not Found'>
           <div className={styles.error}>
                <h1><FaExclamationTriangle/> 404</h1>
                <h4>Sorry, there is nothing here</h4>
                <Link href='/'>Go Back Home</Link>
            </div> 
        </Layout>
    )
}
