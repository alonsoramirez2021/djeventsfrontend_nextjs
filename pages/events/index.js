import Layout from '@/components/Layout';
import Pagination from '@/components/Pagination'
import EventItem from '@/components/EventItem'
import { API_URL, PER_PAGE } from '@/config/index';

export default function EventsPage({events,page,total}) {
  
  return (
        <Layout title='Events'>
        {/*if there are no events, shows a message*/}
        {events.length===0 && <h3>No events to show</h3>}

        {/*Map the JSON data and display every property that belongs to events */}
        {events.map((evt) => (
        <EventItem key ={evt.id} evt={evt} />
        ))}

         {/*Pagination links */} 
        <Pagination page={page} total={total} />
        </Layout>
    )
}

export async function getServerSideProps({ query: { page = 1 } }) {
  // Calculate start page
  const start = +page === 1 ? 0 : (+page - 1) * PER_PAGE

  // Fetch total/count
  const totalRes = await fetch(`${API_URL}/events/count`)
  const total = await totalRes.json()

  // Fetch events
  const eventRes = await fetch(
    `${API_URL}/events?_sort=date:ASC&_limit=${PER_PAGE}&_start=${start}`
  )
  //get the response
  const events = await eventRes.json()

  return {
    //setting the props such as the events data and the pagination config
    props: { events, page: +page, total },
  }
}