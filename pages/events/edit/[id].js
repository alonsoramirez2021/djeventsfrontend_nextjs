import Link from 'next/link'
import Image from 'next/image'
import styles from '@/styles/Form.module.css'
import Layout from '@/components/Layout'
import Modal from '@/components/Modal'
import { useState } from 'react'
import { useRouter } from 'next/router'
import { API_URL } from '@/config/index'
import moment from 'moment'
import {FaImage} from 'react-icons/fa'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import ImageUpload from '@/components/ImageUpload'
import {parseCookies} from '@/helpers/index'


export default function EditEventPage({evt, token}) {
    //array of properties for React Hooks useState
    //we display now the current values on the inputs
    const [values, setValues] = useState({
        name: evt.name,
        performers: evt.performers,
        venue: evt.venue,
        address: evt.address,
        date: evt.date,
        time: evt.time,
        description: evt.description,
      })
      // Top: 0 takes us all the way back to the top of the page
      // Behavior: smooth keeps it smooth!
      const scrollToTop = () => {
        global.window.scrollTo({
          top: 0,
          behavior: "smooth"
        });
        //we set true the modal to be shown
        setShowModal(true);
      };
 
      //React Hooks useState for the image preview of the event
      const [imagePreview, setImagePreview] = useState(evt.image ? evt.image.formats.thumbnail.url : null)
      //Showing modal
      const [showModal, setShowModal] = useState(false)
      //getting the router object
      const router = useRouter()
    
      //prevent it from submitting the form
      const handleSubmit = async (e) => {
        e.preventDefault()
        // Validation to check if all the value fields are empty
        const hasEmptyFields = Object.values(values).some(
            (element) => element === ''
        )
        //check every field but the date one
        if (hasEmptyFields) {
            toast.error('Please fill in all fields')
        }
        
        //we fetch the url, we send our request method, headers and the body
        const res = await fetch(`${API_URL}/events/${evt.id}`, {
            method: 'PUT',
            headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
            },
            body: JSON.stringify(values),
        })
        //checks if the response was good
        if (!res.ok) {
            if (res.status === 403 || res.status === 401) {
                toast.error('Unauthorized')
                return
              }
              toast.error('Something Went Wrong')
        } else {
            //we add the new value and we push it alongside the slug
            const evt = await res.json()
            router.push(`/events/${evt.slug}`)
        }
      }
    //we set all the values according to the name and value
    //this is for every input inside the form when we type in the values
    const handleInputChange = (e) => {
        const { name, value } = e.target
        setValues({ ...values, [name]: value })
      }

    //checking when the image has been uploaded
    const imageUploaded = async (e) => {
        const res = await fetch(`${API_URL}/events/${evt.id}`)
        const data = await res.json()
        setImagePreview(data.image.formats.thumbnail.url)
        setShowModal(false)
      }
    
    return (
            <Layout title='Update Event'>
                <Link href="/events">Go Back</Link>
                <h1>Update Event</h1>
                {/*Displaying toast */}
                <ToastContainer />
                <form onSubmit={handleSubmit} className={styles.form}>
                    <div className={styles.grid}>
                        <div>
                            <label htmlFor='name'>Event Name</label>
                            <input
                            type='text'
                            id='name'
                            name='name'
                            value={values.name}
                            onChange={handleInputChange}
                            />
                        </div>
                        <div>
                            <label htmlFor='performers'>Performers</label>
                            <input
                            type='text'
                            name='performers'
                            id='performers'
                            value={values.performers}
                            onChange={handleInputChange}
                            />
                        </div>
                        <div>
                            <label htmlFor='venue'>Venue</label>
                            <input
                            type='text'
                            name='venue'
                            id='venue'
                            value={values.venue}
                            onChange={handleInputChange}
                            />
                        </div>
                        <div>
                            <label htmlFor='address'>Address</label>
                            <input
                            type='text'
                            name='address'
                            id='address'
                            value={values.address}
                            onChange={handleInputChange}
                            />
                        </div>
                        <div>
                            <label htmlFor='date'>Date</label>
                            <input
                            type='date'
                            name='date'
                            id='date'
                            value={moment(values.date).format('yyyy-MM-DD')}
                            onChange={handleInputChange}
                            />
                        </div>
                        <div>
                            <label htmlFor='time'>Time</label>
                            <input
                            type='text'
                            name='time'
                            id='time'
                            value={values.time}
                            onChange={handleInputChange}
                            />
                        </div>
                    </div>
                    <div>
                        <label htmlFor='description'>Event Description</label>
                        <textarea
                            type='text'
                            name='description'
                            id='description'
                            value={values.description}
                            onChange={handleInputChange}
                        ></textarea>
                    </div>
                    <input type='submit' value='Update Event' className='btn' />
                </form>

                <h2>Event Image</h2>
                {imagePreview ? (
                    <Image src = {imagePreview} height={100} width={170}/>
                ) : 
                <div>
                    <p>No image uploaded</p>
                </div>}
                <div>
                    <button
                    onClick={scrollToTop}
                    className='btn-secondary btn-icon'
                    >
                    <FaImage /> Set Image
                    </button>
                </div>
                <Modal show={showModal} onClose={() => setShowModal(false)}>
                    <ImageUpload evtId={evt.id} imageUploaded={imageUploaded} token={token}/>
                </Modal>
            </Layout>
        )
    }
    
    export async function getServerSideProps({ params: { id }, req }) {

    const {token} = parseCookies(req)
    
    const res = await fetch(`${API_URL}/events/${id}`)
    const evt = await res.json()
    
    
      return {
        props: {
          evt,token
        },
      }
}