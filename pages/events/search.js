import qs from 'qs'
import {Router, useRouter} from 'next/router'
import Link from 'next/link'
import Layout from '@/components/Layout'
import EventItem from '@/components/EventItem'
import { API_URL } from '@/config/index'

export default function SearchPage({events}) {
    //this is for getting and displaying the searched term
    const router = useRouter()
    return (
        <Layout title='Search Results'>
        <Link href='/events'>Go Back</Link>
        <h1>Search Results for {router.query.term}</h1>
        {/*if there are no events, shows a message*/}
        {events.length===0 && <h3>No events to show</h3>}

        {/*Map the JSON data and display every property that belongs to events */}
        {events.map((evt) => (
        <EventItem key ={evt.id} evt={evt} />
        ))}
        </Layout>
    )
}

//filter by terms the search for events
export async function getServerSideProps({query: {term}}){
    //we allow different parameters to be searched according to the json properties name and stringify our json
    const query = qs.stringify({
        _where: {
            //we search with _contains the term we want 
            _or: [
                {name_contains: term},
                {performers_contains: term},
                {description_contains: term},
                {venue_contains: term},
            ]
        }
    })
    
    
    
    //fetching data through the api route with its url and
    const res = await fetch(`${API_URL}/events?${query}`)
    //contains all the json data
    const events = await res.json()
  
    return{
      //object with the props that will be received by the page component
      //shows only 3 events
      props: {events},
    }
  }

  //http://localhost:3000/events/search?term=blackjacks