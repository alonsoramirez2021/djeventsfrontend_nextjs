import Link from 'next/link'
import styles from '@/styles/Form.module.css'
import Layout from "@/components/Layout"
import { useState } from 'react'
import { useRouter } from 'next/router'
import { API_URL } from '@/config/index'
import { ToastContainer, toast } from 'react-toastify';
import {parseCookies} from '@/helpers/index'
import 'react-toastify/dist/ReactToastify.css';

//jwt token is needed
export default function AddEventPage({token}) {
    //array of properties for React Hooks useState
    const [values, setValues] = useState({
        name: '',
        performers: '',
        venue: '',
        address: '',
        date: '',
        time: '',
        description: '',
      })
    
      //getting the router object
      const router = useRouter()
    
      //prevent it from submitting the form
      const handleSubmit = async (e) => {
        e.preventDefault()
        // Validation to check if all the value fields are empty
        const hasEmptyFields = Object.values(values).some(
            (element) => element === ''
        )
        //check every field but the date one
        if (hasEmptyFields) {
            toast.error('Please fill in all fields')
        }
        
        //we fetch the url, we send our request method, headers and the body
        const res = await fetch(`${API_URL}/events`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(values),
          })
        //checks if the response was good
        if (!res.ok) {
            if (res.status === 403 || res.status === 401) {
              toast.error('No token included')
              return
            }
            toast.error('Something went wrong')
        } else {
            //we add the new value and we push it alongside the slug
            const evt = await res.json()
            router.push(`/events/${evt.slug}`)
        }
      }
    //we set all the values according to the name and value
    //this is for every input inside the form when we type in the values
    const handleInputChange = (e) => {
        const { name, value } = e.target
        setValues({ ...values, [name]: value })
      }
    
    return (
        <Layout title='Add New Event'>
            <Link href="/events">Go Back</Link>
            <h1>Add Event</h1>
            {/*Displaying toast */}
            <ToastContainer />
            <form onSubmit={handleSubmit} className={styles.form}>
                <div className={styles.grid}>
                    <div>
                        <label htmlFor='name'>Event Name</label>
                        <input
                        type='text'
                        id='name'
                        name='name'
                        value={values.name}
                        onChange={handleInputChange}
                        />
                    </div>
                    <div>
                        <label htmlFor='performers'>Performers</label>
                        <input
                        type='text'
                        name='performers'
                        id='performers'
                        value={values.performers}
                        onChange={handleInputChange}
                        />
                    </div>
                    <div>
                        <label htmlFor='venue'>Venue</label>
                        <input
                        type='text'
                        name='venue'
                        id='venue'
                        value={values.venue}
                        onChange={handleInputChange}
                        />
                    </div>
                    <div>
                        <label htmlFor='address'>Address</label>
                        <input
                        type='text'
                        name='address'
                        id='address'
                        value={values.address}
                        onChange={handleInputChange}
                        />
                    </div>
                    <div>
                        <label htmlFor='date'>Date</label>
                        <input
                        type='date'
                        name='date'
                        id='date'
                        value={values.date}
                        onChange={handleInputChange}
                        />
                    </div>
                    <div>
                        <label htmlFor='time'>Time</label>
                        <input
                        type='text'
                        name='time'
                        id='time'
                        value={values.time}
                        onChange={handleInputChange}
                        />
                    </div>
                </div>
                <div>
                    <label htmlFor='description'>Event Description</label>
                    <textarea
                        type='text'
                        name='description'
                        id='description'
                        value={values.description}
                        onChange={handleInputChange}
                    ></textarea>
                </div>
                <input type='submit' value='Add Event' className='btn' />
            </form>
        </Layout>
    )
}

export async function getServerSideProps({ req }) {
    const { token } = parseCookies(req)
  
    return {
      props: {
        token,
      },
    }
  }