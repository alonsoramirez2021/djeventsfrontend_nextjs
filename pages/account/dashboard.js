import { useRouter } from 'next/router'
import Layout from '@/components/Layout'
import DashboardEvent from '@/components/DashboardEvent'
import { API_URL } from '@/config/index'
import styles from '@/styles/Dashboard.module.css'
import {parseCookies} from '@/helpers/index'

export default function DashboardPage({events, token}) {

    //console.log(events)
    const router = useRouter()
    //deletes the event
    const deleteEvent = async (id) => {
        if (confirm('Are you sure?')) {
          const res = await fetch(`${API_URL}/events/${id}`, {
            method: 'DELETE',
            //we check the token
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
    
          const data = await res.json()
    
          if (!res.ok) {
            toast.error(data.message)
          } else {
              //reloads the dashboard
            router.reload()
          }
        }
      }
      //visible if you created them
    return (
        <Layout title='User Dashboard'>
            <div className={styles.dash}>
            <h1>Dashboard</h1>
            <h3>My Events</h3>
            {events.map((evt) => (
          <DashboardEvent key={evt.id} evt={evt} handleDelete={deleteEvent}/>
        ))}
            </div>
        </Layout>
    )


}

export async function getServerSideProps({ req }) {
    const { token } = parseCookies(req)
  
    //we send the token if Bearer Token exists in the custom page
    const res = await fetch(`${API_URL}/events/me`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
  
    const events = await res.json()
  
    return {
      props: {
        events, token
      },
    }
  }
