import { createContext, useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { NEXT_URL } from '@/config/index'

//pass the auth state from this component to any other component that requires it
//Calling this returns a pair of components, Provider and Consumer
const AuthContext = createContext()

export const AuthProvider=({children})=>{
    //we set our params
    const [user, setUser]= useState(null)
    const [error, setError]=useState(null)
    //we check if the user is logged in by passing an empty array
    useEffect(()=> checkUserLoggedIn(),[])
    //using the router for redirecting after succesful login or registration
    const router = useRouter()

    //Register user
    const register = async(user) => {
         //we fetch the api through the url
         //we are adding a new user through the register api 
        const res = await fetch(`${NEXT_URL}/api/register`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            //we send the params of the user
            body: JSON.stringify(user),
          })
      
          const data = await res.json()
      
          if (res.ok) {
              //we set the user
            setUser(data.user)
            //this is sent to dashboard
            router.push('/account/dashboard')
          } else {
            setError(data.message)
            setError(null)
          }
    }
    //Login user
    const login = async({email: identifier, password})=>{
        //we fetch the api through the url of our api login
        const res = await fetch(`${NEXT_URL}/api/login`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            //we send the params
            body: JSON.stringify({
              identifier,
              password,
            }),
          })
      
          const data = await res.json()

          if (res.ok) {
              //setting user and redirecting with the changes to dashboard
            setUser(data.user)
            router.push('/account/dashboard')
          } else {
              //setting the error
            setError(data.message)
            setError(null)
          }
      
    }
    //Logout user
    const logout = async()=>{
      //fetching through the logout api page
        const res= await fetch(`${NEXT_URL}/api/logout`,{
            method: 'POST',
        })
        if(res.ok){
            setUser(null)
            //we are redirected to the first page
            router.push('/')
        }
    }

    //Check if user is logged in so the user can be persisted in the local storage 
    const checkUserLoggedIn = async(user)=>{
      //fetching through the user api page
        const res = await fetch(`${NEXT_URL}/api/user`)
        const data = await res.json()

        if(res.ok){
            setUser(data.user)
        }else{
            setUser(null)
        }
    }
    //We bind it to state. So, changing of authenticated value of context does not render the page. 
    //But, whenever authenticated state is changed, it will be rendered.

    // Provider is a Component that allows for one or more Consumers to subscribe to changes.
    return(
        <AuthContext.Provider value={{user,error,register,login,logout}}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContext