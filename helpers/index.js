import cookie from 'cookie'

//parsing cookies from the browser
export function parseCookies(req) {
  return cookie.parse(req ? req.headers.cookie || '' : '')
}